﻿namespace Smart_Kitchen_Program
{
    partial class frm01Main
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtScale1 = new System.Windows.Forms.TextBox();
            this.txtScale3 = new System.Windows.Forms.TextBox();
            this.txtScale5 = new System.Windows.Forms.TextBox();
            this.txtScale7 = new System.Windows.Forms.TextBox();
            this.txtScale9 = new System.Windows.Forms.TextBox();
            this.txtScale2 = new System.Windows.Forms.TextBox();
            this.txtScale4 = new System.Windows.Forms.TextBox();
            this.txtScale6 = new System.Windows.Forms.TextBox();
            this.txtScale8 = new System.Windows.Forms.TextBox();
            this.txtScale10 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.SuspendLayout();
            // 
            // txtScale1
            // 
            this.txtScale1.BackColor = System.Drawing.Color.White;
            this.txtScale1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale1.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale1.Location = new System.Drawing.Point(12, 125);
            this.txtScale1.Name = "txtScale1";
            this.txtScale1.Size = new System.Drawing.Size(250, 96);
            this.txtScale1.TabIndex = 0;
            this.txtScale1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtScale3
            // 
            this.txtScale3.BackColor = System.Drawing.Color.White;
            this.txtScale3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale3.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale3.Location = new System.Drawing.Point(391, 125);
            this.txtScale3.Name = "txtScale3";
            this.txtScale3.Size = new System.Drawing.Size(250, 96);
            this.txtScale3.TabIndex = 1;
            this.txtScale3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtScale5
            // 
            this.txtScale5.BackColor = System.Drawing.Color.White;
            this.txtScale5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale5.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale5.Location = new System.Drawing.Point(770, 125);
            this.txtScale5.Name = "txtScale5";
            this.txtScale5.Size = new System.Drawing.Size(250, 96);
            this.txtScale5.TabIndex = 2;
            this.txtScale5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtScale7
            // 
            this.txtScale7.BackColor = System.Drawing.Color.White;
            this.txtScale7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale7.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale7.Location = new System.Drawing.Point(1149, 125);
            this.txtScale7.Name = "txtScale7";
            this.txtScale7.Size = new System.Drawing.Size(250, 96);
            this.txtScale7.TabIndex = 3;
            this.txtScale7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtScale9
            // 
            this.txtScale9.BackColor = System.Drawing.Color.White;
            this.txtScale9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale9.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale9.Location = new System.Drawing.Point(1528, 125);
            this.txtScale9.Name = "txtScale9";
            this.txtScale9.Size = new System.Drawing.Size(250, 96);
            this.txtScale9.TabIndex = 4;
            this.txtScale9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtScale2
            // 
            this.txtScale2.BackColor = System.Drawing.Color.White;
            this.txtScale2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale2.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale2.Location = new System.Drawing.Point(12, 349);
            this.txtScale2.Name = "txtScale2";
            this.txtScale2.Size = new System.Drawing.Size(250, 96);
            this.txtScale2.TabIndex = 5;
            this.txtScale2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtScale4
            // 
            this.txtScale4.BackColor = System.Drawing.Color.White;
            this.txtScale4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale4.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale4.Location = new System.Drawing.Point(391, 349);
            this.txtScale4.Name = "txtScale4";
            this.txtScale4.Size = new System.Drawing.Size(250, 96);
            this.txtScale4.TabIndex = 6;
            this.txtScale4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtScale6
            // 
            this.txtScale6.BackColor = System.Drawing.Color.White;
            this.txtScale6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale6.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale6.Location = new System.Drawing.Point(770, 349);
            this.txtScale6.Name = "txtScale6";
            this.txtScale6.Size = new System.Drawing.Size(250, 96);
            this.txtScale6.TabIndex = 7;
            this.txtScale6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtScale8
            // 
            this.txtScale8.BackColor = System.Drawing.Color.White;
            this.txtScale8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale8.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale8.Location = new System.Drawing.Point(1149, 349);
            this.txtScale8.Name = "txtScale8";
            this.txtScale8.Size = new System.Drawing.Size(250, 96);
            this.txtScale8.TabIndex = 8;
            this.txtScale8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtScale10
            // 
            this.txtScale10.BackColor = System.Drawing.Color.White;
            this.txtScale10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtScale10.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.txtScale10.Location = new System.Drawing.Point(1528, 349);
            this.txtScale10.Name = "txtScale10";
            this.txtScale10.Size = new System.Drawing.Size(250, 96);
            this.txtScale10.TabIndex = 9;
            this.txtScale10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label2.Location = new System.Drawing.Point(264, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 89);
            this.label2.TabIndex = 11;
            this.label2.Text = "EA";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            this.label2.DoubleClick += new System.EventHandler(this.label2_DoubleClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label3.Location = new System.Drawing.Point(643, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 89);
            this.label3.TabIndex = 12;
            this.label3.Text = "EA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label4.Location = new System.Drawing.Point(1022, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 89);
            this.label4.TabIndex = 13;
            this.label4.Text = "EA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label5.Location = new System.Drawing.Point(1401, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 89);
            this.label5.TabIndex = 14;
            this.label5.Text = "EA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label6.Location = new System.Drawing.Point(1780, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 89);
            this.label6.TabIndex = 15;
            this.label6.Text = "EA";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label7.Location = new System.Drawing.Point(264, 353);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 89);
            this.label7.TabIndex = 16;
            this.label7.Text = "EA";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label8.Location = new System.Drawing.Point(643, 353);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 89);
            this.label8.TabIndex = 17;
            this.label8.Text = "EA";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label9.Location = new System.Drawing.Point(1022, 353);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 89);
            this.label9.TabIndex = 18;
            this.label9.Text = "EA";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label10.Location = new System.Drawing.Point(1401, 353);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 89);
            this.label10.TabIndex = 19;
            this.label10.Text = "EA";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 50F);
            this.label11.Location = new System.Drawing.Point(1780, 353);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 89);
            this.label11.TabIndex = 20;
            this.label11.Text = "EA";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1581, 494);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(197, 62);
            this.button2.TabIndex = 22;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frm01Main
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1904, 605);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtScale10);
            this.Controls.Add(this.txtScale8);
            this.Controls.Add(this.txtScale6);
            this.Controls.Add(this.txtScale4);
            this.Controls.Add(this.txtScale2);
            this.Controls.Add(this.txtScale9);
            this.Controls.Add(this.txtScale7);
            this.Controls.Add(this.txtScale5);
            this.Controls.Add(this.txtScale3);
            this.Controls.Add(this.txtScale1);
            this.Name = "frm01Main";
            this.Text = "Smart Kitchen";
            this.Load += new System.EventHandler(this.frm01Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtScale1;
        private System.Windows.Forms.TextBox txtScale3;
        private System.Windows.Forms.TextBox txtScale5;
        private System.Windows.Forms.TextBox txtScale7;
        private System.Windows.Forms.TextBox txtScale9;
        private System.Windows.Forms.TextBox txtScale2;
        private System.Windows.Forms.TextBox txtScale4;
        private System.Windows.Forms.TextBox txtScale6;
        private System.Windows.Forms.TextBox txtScale8;
        private System.Windows.Forms.TextBox txtScale10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button2;
        private System.IO.Ports.SerialPort serialPort1;
    }
}

