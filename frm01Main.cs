﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Common_Library;
namespace Smart_Kitchen_Program
{
    public partial class frm01Main : Form
    {
        System.IO.Ports.SerialPort SP0 = new System.IO.Ports.SerialPort();
        System.IO.Ports.SerialPort SP1 = new System.IO.Ports.SerialPort();
        System.IO.Ports.SerialPort SP2 = new System.IO.Ports.SerialPort();
        System.IO.Ports.SerialPort SP3 = new System.IO.Ports.SerialPort();
        System.IO.Ports.SerialPort SP4 = new System.IO.Ports.SerialPort();
        System.IO.Ports.SerialPort SP5 = new System.IO.Ports.SerialPort();
        System.IO.Ports.SerialPort SP6 = new System.IO.Ports.SerialPort();
        System.IO.Ports.SerialPort SP7 = new System.IO.Ports.SerialPort();
        System.IO.Ports.SerialPort SP8 = new System.IO.Ports.SerialPort();
        System.IO.Ports.SerialPort SP9 = new System.IO.Ports.SerialPort();
        List<System.IO.Ports.SerialPort> SP = new List<System.IO.Ports.SerialPort>();
        List<TextBox> lsttxtScale = new List<TextBox>();
        string strFolder;
        public double weight;
        System.Timers.Timer SendMsg;
        public frm01Main()
        {
            
            InitializeComponent();
            SP.Add(SP0); SP.Add(SP1); SP.Add(SP2); SP.Add(SP3); SP.Add(SP4); SP.Add(SP5); SP.Add(SP6); SP.Add(SP7); SP.Add(SP8); SP.Add(SP9);
            lsttxtScale.Add(txtScale1); lsttxtScale.Add(txtScale2); lsttxtScale.Add(txtScale3); lsttxtScale.Add(txtScale4); lsttxtScale.Add(txtScale5); lsttxtScale.Add(txtScale6); lsttxtScale.Add(txtScale7); lsttxtScale.Add(txtScale8); lsttxtScale.Add(txtScale9); lsttxtScale.Add(txtScale10);
            for(int i = 0;i<lsttxtScale.Count;i++)
            {
                lsttxtScale[i].Click += txtScaleClick;
            }
            strFolder = Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData) + "\\SMART_KITCHEN_KJ\\";
            
        }

        private void txtScaleClick(object sender, EventArgs e)
        {
            string name = ((TextBox)sender).Name;
            name = name.Substring(8);
            frm02Setting FS = new frm02Setting(this);
            FS.idx = int.Parse(name);
            FS.ShowDialog();

        }

        private void frm01Main_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < SP.Count; i++)
            {
                SP[i].Close();
                SP[i].BaudRate = 9600;
                SP[i].DataBits = 7;
                SP[i].StopBits = System.IO.Ports.StopBits.One;
                SP[i].Parity = System.IO.Ports.Parity.Even;
                //SP[i].ReadTimeout = 300;
                SP[i].DtrEnable = true;
                SP[i].RtsEnable = true;
                //SP[i].ReceivedBytesThreshold = 17;
                //SP[i].DataReceived += Scale_DataReceived;
                
            }
            List<string> PortList = new List<string>();
            foreach(string s in System.IO.Ports.SerialPort.GetPortNames())
            {
                PortList.Add(s);
            }
            for(int i = 0;i<PortList.Count;i++)
            {
                SP[i].PortName = PortList[i];
                SP[i].Open();
            }
            label1.Focus();
            label1.Text = "";
            for (int i = 0; i < lsttxtScale.Count; i++) lsttxtScale[i].BorderStyle = BorderStyle.None;
            SendMsg = new System.Timers.Timer();

            SendMsg.Interval = 500;
            SendMsg.Enabled = true;
            SendMsg.Elapsed += TmedMsg_Elapsed;
        }

        private void TmedMsg_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string ReadStr;
            byte[] ReadByte = new byte[17];
            int ConList = 0;

            for (int i = 0; i < SP.Count; i++)
            {
                if (SP[i].IsOpen == false) continue;
                SP[i].Write("Q\r\n");
                try
                {
                    
                    ReadStr = SP[i].ReadExisting();
                    if (ReadStr.Length != 17) continue;

                    if (ReadStr.Substring(0, 2) != "ST") continue;
                    ReadStr = ReadStr.Substring(3, 9);
                    if (Information.IsNumeric(ReadStr) == false) continue;
                    Console.WriteLine(ReadStr.Length.ToString() + ", " + ReadStr);
                    if (weight != 0) ReadStr = (Math.Truncate(double.Parse(ReadStr) / weight)).ToString();
                    if(double.Parse(ReadStr) < 0)
                    {
                        SendMsg.Enabled = false;
                        clsCommon_Library.NSleep(300);
                        SP[i].Write("Z\r\n");
                        SendMsg.Enabled = true;
                    }
                    SetScale(lsttxtScale[ConList], double.Parse(ReadStr));
                    ConList++;
                }
                catch 
                {

                }

                
            }

            //throw new NotImplementedException();
        }

        private void Scale_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {

            string ReadStr;
            byte[] ReadByte = new byte[17] ;
            int ConList = 0;

            for(int i = 0;i < SP.Count;i++)
            {
                if (sender.Equals(SP[i]) == true)
                {
                    
                    //SP[i].Read(ReadByte, 0, ReadByte.Length);
                   ReadStr =  SP[i].ReadExisting();
                   if (ReadStr.Length != 17) continue;

                    if (ReadStr.Substring(0, 2) != "ST") continue;
                    ReadStr = ReadStr.Substring(4, 8);
                    if (Information.IsNumeric(ReadStr) == false) continue;
                   Console.WriteLine(ReadStr.Length.ToString() + ", " + ReadStr);
                    if (weight != 0) ReadStr = (Math.Truncate( double.Parse(ReadStr) / weight)).ToString();
                    SetScale(lsttxtScale[ConList], double.Parse(ReadStr));
                    ConList++;
                }
            }
            
            
        }
        delegate void CrossThreadSafetySetText(TextBox ctl, double VALUE);
        private void SetScale(TextBox ctl, double VALUE)
        {

            /*
             * InvokeRequired 속성 (Control.InvokeRequired, MSDN)
             *   짧게 말해서, 이 컨트롤이 만들어진 스레드와 현재의 스레드가 달라서
             *   컨트롤에서 스레드를 만들어야 하는지를 나타내는 속성입니다.  
             * 
             * InvokeRequired 속성의 값이 참이면, 컨트롤에서 스레드를 만들어 텍스트를 변경하고,
             * 그렇지 않은 경우에는 그냥 변경해도 아무 오류가 없기 때문에 텍스트를 변경합니다.
             */
            if (ctl.InvokeRequired)
                ctl.Invoke(new CrossThreadSafetySetText(SetScale), ctl, VALUE);
            else
            {
                ctl.Text = VALUE.ToString();
                label1.Focus();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TmedMsg_Elapsed(this, null);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label2_DoubleClick(object sender, EventArgs e)
        {
            SendMsg.Enabled = false;
            clsCommon_Library.NSleep(500);
            SP[2].Write("Z\r\n");
            SendMsg.Enabled = true;
        }
    }
}
