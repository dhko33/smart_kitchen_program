﻿namespace Smart_Kitchen_Program
{
    partial class frm02Setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nuddishWeight = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nudtribcount = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDishName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nuddishWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudtribcount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // nuddishWeight
            // 
            this.nuddishWeight.DecimalPlaces = 2;
            this.nuddishWeight.Font = new System.Drawing.Font("맑은 고딕", 40F);
            this.nuddishWeight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nuddishWeight.Location = new System.Drawing.Point(413, 120);
            this.nuddishWeight.Name = "nuddishWeight";
            this.nuddishWeight.Size = new System.Drawing.Size(299, 78);
            this.nuddishWeight.TabIndex = 0;
            this.nuddishWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 30F);
            this.label1.Location = new System.Drawing.Point(448, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(294, 54);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dish Weight(g)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 30F);
            this.label2.Location = new System.Drawing.Point(1202, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(183, 54);
            this.label2.TabIndex = 5;
            this.label2.Text = "부족개수";
            // 
            // nudtribcount
            // 
            this.nudtribcount.Font = new System.Drawing.Font("맑은 고딕", 40F);
            this.nudtribcount.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudtribcount.Location = new System.Drawing.Point(1144, 120);
            this.nudtribcount.Name = "nudtribcount";
            this.nudtribcount.Size = new System.Drawing.Size(299, 78);
            this.nudtribcount.TabIndex = 4;
            this.nudtribcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 30F);
            this.label3.Location = new System.Drawing.Point(853, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(183, 54);
            this.label3.TabIndex = 7;
            this.label3.Text = "위험개수";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Font = new System.Drawing.Font("맑은 고딕", 40F);
            this.numericUpDown1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown1.Location = new System.Drawing.Point(795, 120);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(299, 78);
            this.numericUpDown1.TabIndex = 6;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 30F);
            this.label4.Location = new System.Drawing.Point(106, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(197, 54);
            this.label4.TabIndex = 8;
            this.label4.Text = "접시 이름";
            // 
            // txtDishName
            // 
            this.txtDishName.Font = new System.Drawing.Font("맑은 고딕", 40F);
            this.txtDishName.Location = new System.Drawing.Point(12, 120);
            this.txtDishName.Name = "txtDishName";
            this.txtDishName.Size = new System.Drawing.Size(384, 78);
            this.txtDishName.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.button1.Location = new System.Drawing.Point(696, 119);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 40);
            this.button1.TabIndex = 10;
            this.button1.Text = "▲";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("맑은 고딕", 15F);
            this.button2.Location = new System.Drawing.Point(696, 159);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(65, 40);
            this.button2.TabIndex = 11;
            this.button2.Text = "▼";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("맑은 고딕", 30F);
            this.btnSubmit.Location = new System.Drawing.Point(1577, 229);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(207, 75);
            this.btnSubmit.TabIndex = 12;
            this.btnSubmit.Text = "확인";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 30F);
            this.label5.Location = new System.Drawing.Point(1568, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 54);
            this.label5.TabIndex = 14;
            this.label5.Text = "영점";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.DecimalPlaces = 2;
            this.numericUpDown2.Font = new System.Drawing.Font("맑은 고딕", 40F);
            this.numericUpDown2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown2.Location = new System.Drawing.Point(1470, 119);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(299, 78);
            this.numericUpDown2.TabIndex = 13;
            this.numericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // frm02Setting
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1971, 359);
            this.ControlBox = false;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtDishName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nudtribcount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nuddishWeight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frm02Setting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frm02Setting_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nuddishWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudtribcount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nuddishWeight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudtribcount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDishName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
    }
}