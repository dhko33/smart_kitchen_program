﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Smart_Kitchen_Program
{
    public partial class frm02Setting : Form
    {
        frm01Main FM;
        public int idx;
        public frm02Setting(frm01Main fm)
        {
            FM = fm;
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDishWUp_Click(object sender, EventArgs e)
        {
            nuddishWeight.Value = nuddishWeight.Value + (decimal)0.01;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            nuddishWeight.Value = nuddishWeight.Value + (decimal)0.01;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            FM.weight = (double)nuddishWeight.Value;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (nuddishWeight.Value <= (decimal)0.01) return;
            nuddishWeight.Value = nuddishWeight.Value - (decimal)0.01;
        }

        private void frm02Setting_Load(object sender, EventArgs e)
        {
            
        }
    }
}
